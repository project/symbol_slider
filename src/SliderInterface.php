<?php

namespace Drupal\symbol_slider;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Slider entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup symbol_slider
 */
interface SliderInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {
}
