<?php

namespace Drupal\symbol_slider\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;

/**
 * Form controller for the symbol_slider entity edit forms.
 *
 * @ingroup symbol_slider
 */
class SliderForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\symbol_slider\Entity\Slider $entity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;
    $form['langcode'] = [
      '#title' => $this->t('Language'),
      '#type' => 'language_select',
      '#default_value' => $entity->getUntranslated()->language()->getId(),
      '#languages' => Language::STATE_ALL,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.symbol_slider.collection');
    $entity = $this->getEntity();
    $entity->save();
  }

}
